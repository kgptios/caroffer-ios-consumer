//
//  RBCDetailViewController.h
//  carOffer
//
//  Created by Balaganesan on 1/15/14.
//  Copyright (c) 2014 Balaganesan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBCDetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
