//
//  RBCMasterViewController.h
//  carOffer
//
//  Created by Balaganesan on 1/15/14.
//  Copyright (c) 2014 Balaganesan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RBCDetailViewController;

@interface RBCMasterViewController : UITableViewController

@property (strong, nonatomic) RBCDetailViewController *detailViewController;

@end
