//
//  main.m
//  carOffer
//
//  Created by Balaganesan on 1/15/14.
//  Copyright (c) 2014 Balaganesan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RBCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RBCAppDelegate class]));
    }
}
