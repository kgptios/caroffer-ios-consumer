//
//  RBCAppDelegate.h
//  carOffer
//
//  Created by Balaganesan on 1/15/14.
//  Copyright (c) 2014 Balaganesan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
